# RASM v2 #
This repository is for the continued development of the software system for the RASM (Robotic Adaptive Screen Mount).  The RASM v2 is a second-year senior design project at the University of Utah (Mechanical Engineering department).  The faculty advisor is Dr. Andrew Merryweather.

## Structure ##
This repository is structured as a usual non-built ROS catkin workspace with the addition of a top-level resources directory.

## Build Directions ##
Clone this repository onto a machine with ROS Kinetic or Lunar installed.  Run $catkin_make on the top-level directory to build all nodes.
