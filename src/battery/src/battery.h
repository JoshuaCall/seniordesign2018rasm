#ifndef BATTERY_INCLUDED
#define BATTERY_INCLUDED

#include <stdint.h>
#include <battery/GetVoltage.h>
#include <battery/GetPercent.h>
#include <battery/GetTime.h>


/**
 * This class communicates with the power distribution board and battery charger
 * in order to monitor the state of the battery and it's charger.
 */
class BatterySentinel
{
private:

public:
    BatterySentinel(); // populate fields via ROS parameter server

    void start();
    void close();
    
    /**
     * Returns the current battery voltage.
     */
    bool get_voltage(battery::GetVoltage::Request &_, 
                     battery::GetVoltage::Response &voltage);

    /**
     * Returns the approximate battery charge level as a percentage.
     */
    bool get_percent_charged(battery::GetPercent::Request &_, 
                             battery::GetPercent::Response &percent);

    /**
     * Returns the estimated seconds remaining until the battery is 'dead'.
     */
    bool get_time_remaining(battery::GetTime::Request &_, 
                            battery::GetTime::Response &time);
};

#endif
