#include "ros/ros.h"
#include "battery.h"


int main(int argc, char **argv)
{
    // specify node name and get a handle for this node
    ros::init(argc, argv, "sentinel");
    ros::NodeHandle nh;

    // topic publishers
    // none

    // service clients
    // none

    // initialize node backend
    BatterySentinel sentinel;
    sentinel.start();

    // topic subscribers
    // none

    // accessor service servers
    ros::ServiceServer voltage_get = nh.advertiseService(
        "get_voltage", &BatterySentinel::get_voltage, &sentinel);
    ros::ServiceServer percent_get = nh.advertiseService(
        "get_percent_charged", &BatterySentinel::get_percent_charged, &sentinel);
    ros::ServiceServer time_get = nh.advertiseService(
        "get_time_remaining", &BatterySentinel::get_time_remaining, &sentinel);

    // modifier service servers
    // none

    ros::spin();
    sentinel.close();
}
