#include "battery.h"

BatterySentinel::BatterySentinel()
{

}

void BatterySentinel::start()
{

}

void BatterySentinel::close()
{

}

bool BatterySentinel::get_voltage(battery::GetVoltage::Request &_, 
                     battery::GetVoltage::Response &voltage)
{
    return false;
}

bool BatterySentinel::get_percent_charged(battery::GetPercent::Request &_, 
                     battery::GetPercent::Response &percent)
{
    return false;
}

bool BatterySentinel::get_time_remaining(battery::GetTime::Request &_, 
                     battery::GetTime::Response &time)
{
    return false;
}
