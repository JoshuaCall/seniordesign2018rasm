#include "ros/ros.h"
#include "joint_control.h"
#include "joint_control/Mode.h"
#include "battery/GetVoltage.h"


int main(int argc, char **argv)
{
    // specify node name and get a handle for this node
    ros::init(argc, argv, "controller");
    ros::NodeHandle nh;

    // topic publishers
    ros::Publisher mode_pub = nh.advertise<joint_control::Mode>("mode", 10);

    // service clients
    ros::ServiceClient getvoltage_client = 
        nh.serviceClient<battery::GetVoltage>("get_voltage");

    // initialize node backend
    PubsAndClients temp = {mode_pub, getvoltage_client};
    JointController jcontroller(temp);
    jcontroller.start();

    // topic subscribers
    ros::Subscriber objectpose_sub = nh.subscribe(
        "object_pose", 1, &JointController::update_objectpose, &jcontroller);

    // service servers
    // accessors
    ros::ServiceServer mode_get = nh.advertiseService(
        "get_mode", &JointController::get_mode, &jcontroller);
    ros::ServiceServer screenpose_get = nh.advertiseService(
        "get_screen_pose", &JointController::get_screen_pose, &jcontroller);
    ros::ServiceServer jointpositions_get = nh.advertiseService(
        "get_joint_positions", &JointController::get_joint_positions, &jcontroller);
    ros::ServiceServer pwmvalues_get = nh.advertiseService(
        "get_pwm_values", &JointController::get_pwm_values, &jcontroller);

    // modifiers
    ros::ServiceServer screenpose_set = nh.advertiseService(
        "set_screen_pose", &JointController::set_screen_pose, &jcontroller);
    ros::ServiceServer faceselection_set = nh.advertiseService(
        "set_joint_positions", &JointController::set_joint_positions, &jcontroller);

    ros::spin();
    jcontroller.close();
}
