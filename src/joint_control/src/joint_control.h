#ifndef JOINT_CONTROL_INCLUDED
#define JOINT_CONTROL_INCLUDED

#include "ros/ros.h"
#include "joint_control/Mode.h"
#include "joint_control/GetMode.h"
#include "joint_control/GetPose.h"
#include "joint_control/GetPositions.h"
#include "joint_control/GetPwms.h"
#include "joint_control/SetPose.h"
#include "joint_control/SetPositions.h"
#include "image_process/ObjectPose.h"


/**
 * A container for all the topic publishers and service clients used by the
 * controller.
 */
struct PubsAndClients
{
    ros::Publisher mode_pub;
    ros::ServiceClient getvoltage_client;
};


/**
 * This class implements a configurable controller for all actuators of the RASM.
 */
class JointController
{
private:
    ros::Publisher mode_pub;
    ros::ServiceClient getvoltage_client;

public:
    JointController(PubsAndClients pc);

    void start();
    void close();

    void update_objectpose(const image_process::ObjectPose::ConstPtr &msg);

    bool get_mode(joint_control::GetMode::Request &_,
                  joint_control::GetMode::Response &mode);
    
    bool get_screen_pose(joint_control::GetPose::Request &_,
                         joint_control::GetPose::Response &pose);

    bool get_joint_positions(joint_control::GetPositions::Request &_,
                             joint_control::GetPositions::Response &postions);
    
    bool get_pwm_values(joint_control::GetPwms::Request &_,
                        joint_control::GetPwms::Response &pwms);
    
    bool set_screen_pose(joint_control::SetPose::Request &pose,
                         joint_control::SetPose::Response &_);
    
    bool set_joint_positions(joint_control::SetPositions::Request &positions,
                             joint_control::SetPositions::Response &_);
};

#endif
