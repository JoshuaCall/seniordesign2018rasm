#include "joint_control.h"


JointController::JointController(PubsAndClients pc)
: mode_pub(pc.mode_pub)
, getvoltage_client(pc.getvoltage_client)
{

}

void JointController::start()
{

}

void JointController::close()
{

}

void JointController::update_objectpose(const image_process::ObjectPose::ConstPtr &msg)
{

}

bool JointController::get_mode(joint_control::GetMode::Request &_,
                    joint_control::GetMode::Response &mode)
{
    
}

bool JointController::get_screen_pose(joint_control::GetPose::Request &_,
                    joint_control::GetPose::Response &pose)
{
    
}

bool JointController::get_joint_positions(joint_control::GetPositions::Request &_,
                    joint_control::GetPositions::Response &postions)
{
    
}

bool JointController::get_pwm_values(joint_control::GetPwms::Request &_,
                    joint_control::GetPwms::Response &pwms)
{
    
}

bool JointController::set_screen_pose(joint_control::SetPose::Request &pose,
                    joint_control::SetPose::Response &_)
{
    
}

bool JointController::set_joint_positions(joint_control::SetPositions::Request &positions,
                    joint_control::SetPositions::Response &_)
{

}
