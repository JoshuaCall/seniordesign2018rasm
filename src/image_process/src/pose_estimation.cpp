#include "pose_estimation.h"


FaceTracker::FaceTracker()
{

}

MarkerTracker::MarkerTracker()
{

}

ObjectTracker::ObjectTracker(ros::Publisher &objectpose_pub)
: objectpose_pub(objectpose_pub)
, faceTracker()
, markerTracker()
{
    // set all poses to 1 unit length in front and down and facing towards 
    // screen camera
    facepose.fill(0);
    facepose[0] = 1;
    facepose[3] = -1;
    facepose[4] = -1;

    markerpose.fill(0);
    markerpose[0] = 1;
    markerpose[3] = -1;
    markerpose[4] = -1;

    objectpose.fill(0);
    objectpose[0] = 1;
    objectpose[3] = -1;
    objectpose[4] = -1;

    min_required_change.fill(0);  // set from param server
}

void ObjectTracker::start()
{

}

void ObjectTracker::close()
{
    
}

bool ObjectTracker::get_object_pose(image_process::GetPose::Request &_,
                     image_process::GetPose::Response &pose)
{
    return false;
}

bool ObjectTracker::get_face_pose(image_process::GetPose::Request &_,
                   image_process::GetPose::Response &pose)
{
    return false;
}

bool ObjectTracker::get_marker_pose(image_process::GetPose::Request &_,
                     image_process::GetPose::Response &pose)
{
    return false;
}

bool ObjectTracker::set_object_selection(image_process::ObjectSelect::Request &mode,
                          image_process::ObjectSelect::Response &_)
{
    return false;
}

bool ObjectTracker::set_face_selection(image_process::FaceSelect::Request &mode,
                        image_process::FaceSelect::Response &_)
{
    return false;
}

bool ObjectTracker::set_marker_selection(image_process::MarkerSelect::Request &mode,
                          image_process::MarkerSelect::Response &_)
{
    return false;
}

bool ObjectTracker::set_marker_type(image_process::SetMarker::Request &type,
                     image_process::SetMarker::Response &_)
{
    return false;
}
