#ifndef POSE_TRACKING_INCLUDED
#define POSE_TRACKING_INCLUDED

#include "ros/ros.h"
#include "image_process/ObjectPose.h"
#include "image_process/GetPose.h"
#include "image_process/ObjectSelect.h"
#include "image_process/FaceSelect.h"
#include "image_process/MarkerSelect.h"
#include "image_process/SetMarker.h"
#include <image_transport/image_transport.h>
#include <cv_bridge/cv_bridge.h>
// opencv includes
#include <opencv2/opencv.hpp>
// Message filter includes
#include <message_filters/subscriber.h>
#include <message_filters/synchronizer.h>
#include <message_filters/sync_policies/approximate_time.h>
#include <sensor_msgs/Image.h>
#include <sensor_msgs/CameraInfo.h>
#include <image_transport/subscriber_filter.h>
//// dlib includes
#include <dlib/opencv.h>
#include <dlib/image_processing/frontal_face_detector.h>
#include <dlib/image_processing/render_face_detections.h>
#include <dlib/image_processing.h>
#include <dlib/gui_widgets.h>
#include <dlib/image_io.h>

#include <pcl_ros/point_cloud.h>
#include <pcl/point_types.h>
#include <pcl/registration/icp.h>
#include <limits>

#include <geometry_msgs/Pose.h>
#include <geometry_msgs/PoseStamped.h>
#include <tf/transform_broadcaster.h>
#include <array>

typedef std::array<float,7> Pose;

typedef image_transport::SubscriberFilter imgSuber;
typedef message_filters::sync_policies::ApproximateTime
             <sensor_msgs::Image,sensor_msgs::PointCloud2> MySyncPolicy_;
typedef dlib::rgb_pixel img_type;

typedef pcl::PointXYZRGB PtXYZRGB;
typedef pcl::PointCloud<PtXYZRGB> PCXYZRGB;

/**
 * This class is for identification of a person's facial pose from an image.
 */
class FaceTracker
{
private:


public:
    FaceTracker();


};


/**
 * This class is for finding the pose of a specific visual marker within an image.
 */
class MarkerTracker
{
private:


public:
    MarkerTracker();


};


/**
 * This class determines a pose from two other potentially present poses: a
 * facial pose and a marker pose.
 */
class ObjectTracker
{
private:
    ros::Publisher objectpose_pub;

    FaceTracker faceTracker;
    MarkerTracker markerTracker;

    Pose facepose;
    Pose markerpose;
    Pose objectpose;
    Pose min_required_change;

    void update_facepose(Pose &pose);
    void update_markerpose(Pose &pose);

public:
    ObjectTracker(ros::Publisher &objectpose_pub);

    void start();
    void close();
    
    bool get_object_pose(image_process::GetPose::Request &_,
                         image_process::GetPose::Response &pose);

    bool get_face_pose(image_process::GetPose::Request &_,
                       image_process::GetPose::Response &pose);

    bool get_marker_pose(image_process::GetPose::Request &_,
                         image_process::GetPose::Response &pose);

    bool set_object_selection(image_process::ObjectSelect::Request &mode,
                              image_process::ObjectSelect::Response &_);

    bool set_face_selection(image_process::FaceSelect::Request &mode,
                            image_process::FaceSelect::Response &_);

    bool set_marker_selection(image_process::MarkerSelect::Request &mode,
                              image_process::MarkerSelect::Response &_);

    bool set_marker_type(image_process::SetMarker::Request &type,
                         image_process::SetMarker::Response &_);
};

#endif
