#include "ros/ros.h"
#include "pose_estimation.h"
#include "image_process/ObjectPose.h"


int main(int argc, char **argv)
{
    // specify node name and get a handle for this node
    ros::init(argc, argv, "tracker");
    ros::NodeHandle nh;

    // topic publishers
    ros::Publisher objectpose_pub = 
        nh.advertise<image_process::ObjectPose>("object_pose", 1);

    // service clients
    // none

    // initialize node backend
    ObjectTracker objtracker(objectpose_pub);
    objtracker.start();

    // topic subscribers
    // none
     
    // accessor service servers
    ros::ServiceServer objectpose_get = nh.advertiseService(
        "get_object_pose", &ObjectTracker::get_object_pose, &objtracker);
    ros::ServiceServer facepose_get = nh.advertiseService(
        "get_face_pose", &ObjectTracker::get_face_pose, &objtracker);
    ros::ServiceServer markerpose_get = nh.advertiseService(
        "get_marker_pose", &ObjectTracker::get_marker_pose, &objtracker);
    
    // modifier service servers
    ros::ServiceServer objectselection_set = nh.advertiseService(
        "set_object_selection", &ObjectTracker::set_object_selection, &objtracker);
    ros::ServiceServer faceselection_set = nh.advertiseService(
        "set_face_selection", &ObjectTracker::set_face_selection, &objtracker);
    ros::ServiceServer markerselection_set = nh.advertiseService(
        "set_marker_selection", &ObjectTracker::set_marker_selection, &objtracker);
    ros::ServiceServer markertype_set = nh.advertiseService(
        "set_marker_type", &ObjectTracker::set_marker_type, &objtracker);

    ros::spin();
    objtracker.close();
}
